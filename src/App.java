import java.util.ArrayList;
import com.dev.Country;
import com.dev.Region;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        ArrayList<Country> countries = new ArrayList<>();
        
        
        ArrayList<Region> regionsVietnam = new ArrayList<>();
        regionsVietnam.add(new Region("HN", "Hà Nội"));
        regionsVietnam.add(new Region("HCM", "Hồ Chí Minh"));
        Country vietnam = new Country("VN", "Việt Nam", regionsVietnam);
        // Thêm các country vào danh sách
        countries.add(vietnam);
        countries.add(new Country("US", "United States", new ArrayList<>()));
        countries.add(new Country("CAN", "Canada", new ArrayList<>()));
        
        // In ra danh sách các country
        for (Country country : countries) {
            System.out.println("----------gạch ngang cho dễ nhìn--------");
            System.out.println("Country code: " + country.getCountryCode());
            System.out.println("Country name: " + country.getCountryName());
            if (country.getCountryName().equals("Việt Nam")) {
                System.out.println("List of regions in VietNam: ");
                for (Region region : country.getRegions()) {
                    System.out.println("Region code: " + region.getRegionCode());
                    System.out.println("Region name: " + region.getRegionName());
                }
            }
        }



}

}