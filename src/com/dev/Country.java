package com.dev;
import java.util.ArrayList;


public class Country {
    private String countryCode;
        private String countryName;
        private ArrayList<Region> regions;
        
        public Country(String countryCode, String countryName, ArrayList<Region> regions) {
            this.countryCode = countryCode;
            this.countryName = countryName;
            this.regions = regions;
        }
        
        public String getCountryCode() {
            return countryCode;
        }
        
        public String getCountryName() {
            return countryName;
        }
        
        public ArrayList<Region> getRegions() {
            return regions;
        }
}
